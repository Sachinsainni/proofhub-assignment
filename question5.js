// 5, Write a program in javascript for following

// a, declare array

const array = []

// b, add item

array.push(item)

// c, delete item

array.pop()  // delete the last index element of the array
array.shift()  // delete the last index element of the array

// d, add at particular index

array.splice(0, 0, item) // splice function use to add element in particular index 

// another way 
for (let index = array.length; index > 0; index--) {
    array[index] = array[index - 1]
}
array[index] = item

// e, delete from particular index

array.splice(1, 1) // Splice also used for to remove or delete the element for particular index. first parameter have index Number and second parameter have how many element you want remove from the index.  

