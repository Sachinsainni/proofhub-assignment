let [millisecond,second,minute,hour] = [0,0,0,0];

function displayTimer() {
	millisecond += 10;

	if (millisecond == 1000) {
		millisecond = 0;
		second++;

		if (second == 60) {
			second = 0;
			minute++;

			if (minute == 60) {
				minute = 0;
				hour++;
			}
		}
	}
	let h = hour < 10 ? "0" + hour : hour;
	let m = minute < 10 ? "0" + minute : minute;
	let s = second < 10 ? "0" + second : second;
	let ms = millisecond < 10 ? "00" + millisecond : millisecond < 100 ? "0" + millisecond : millisecond;
   
	console.log(` ${h} : ${m} : ${s} : ${ms}`);
}
displayTimer();