// 4, Consider the following JSON:-
// Print user details in a list format using javascript/jquery using table tag

let data = {
    "employees":
        [
            {
                "first_name": "Amit",
                "last_name": "Sharma",
                "department": "SEO"
            },
            {
                "first_name": "Vineet",
                "last_name": "Kumar",
                "department": "Accounts"
            },
            {
                "first_name": "Ajay",
                "last_name": "Thakur",
                "department": "QA"
            },
            {
                "first_name": "Chandra",
                "last_name": "Sharma",
                "department": "Designer"
            }
        ]
}


let details = data.employees;

for (let index = 0; index < details.length; index++) {
    console.table(details[index]);
}