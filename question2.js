// 2, Write a program to get a sum of even and odd numbers between 1-100./

let evenNo = 0;
let oddNo = 0;

for (let index = 0; index <= 100 ; index++){
    if(index%2==0){
        evenNo += index
    }else{
        oddNo += index
    }
}

console.log(evenNo);
console.log(oddNo);